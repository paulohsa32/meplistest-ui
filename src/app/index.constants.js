/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('meplistest1')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
