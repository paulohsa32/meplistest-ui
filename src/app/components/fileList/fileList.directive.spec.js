(function() {
    'use strict';

    describe('directive mepFileList', function() {
        //var vm;
        var el, scope;

        beforeEach(module('meplistest1'));
        beforeEach(inject(function($compile, $rootScope) {

            el = angular.element('<mep-file-list title="Files" storage-data="storageData" files="file"></mep-file-list>');

            scope = $rootScope.$new();
            scope.file = [{
                name: 'File 1',
                path: '.../folder/file_example_1.jpg',
                thumb: 'https://lh4.googleusercontent.com/-115_BjIc-w0/VKwr3Z2GkuI/AAAAAAAAGCE/x9j8kkCrzhM/s1600/files_app_icon_256.png',
                lastModification: new Date,
                size: 50,
                uploadDate: new Date
            }]
            scope.storageData = {
                limit: 100000,
                used: 5000
            };

            $compile(el)(scope);
            scope.$digest();
            //vm = el.isolateScope().vm;
        }));

        it('should be compiled', function() {
            expect(el.html()).not.toEqual(null);
        });
    });
})();
