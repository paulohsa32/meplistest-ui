(function() {
    "use strict";

    angular
        .module("meplistest1")
        .directive('mepFileList', mepFileList);

    function mepFileList() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/fileList/fileList.template.html',
            scope: {
                files: '=',
                title: '=',
                storageData: '='
            },
            link: mepFileListLink
        };
        return directive;
    }

    function mepFileListLink(scope) {

        scope.formatBytes = formatBytes;

        if (scope.storageData) {
            scope.storageData.percentage = (scope.storageData.used * 100) / scope.storageData.limit;
        }

        function formatBytes(bytes, decimals) {
            if (bytes == 0) return '0 Byte';
            var k = 1000;
            var dm = decimals + 1 || 3;
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
            var i = Math.floor(Math.log(bytes) / Math.log(k));
            return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
        }

    }

})();
