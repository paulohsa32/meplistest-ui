(function() {
  'use strict';

  angular
    .module('meplistest1', ['ngAnimate', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ngMaterial', 'toastr']);

})();
