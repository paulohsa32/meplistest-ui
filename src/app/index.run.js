(function() {
  'use strict';

  angular
    .module('meplistest1')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
