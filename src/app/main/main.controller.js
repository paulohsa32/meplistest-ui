(function() {
    'use strict';

    angular
        .module('meplistest1')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController() {
        var vm = this;

        vm.fileList = [{
            name: 'File 1',
            path: '.../folder/file_example_1.jpg',
            thumb: 'https://lh4.googleusercontent.com/-115_BjIc-w0/VKwr3Z2GkuI/AAAAAAAAGCE/x9j8kkCrzhM/s1600/files_app_icon_256.png',
            lastModification: new Date,
            size: 123000,
            uploadDate: new Date
        },{
            name: 'File 2',
            path: '.../folder/file_example_2.jpg',
            thumb: 'https://lh4.googleusercontent.com/-115_BjIc-w0/VKwr3Z2GkuI/AAAAAAAAGCE/x9j8kkCrzhM/s1600/files_app_icon_256.png',
            lastModification: new Date,
            size: 1230000,
            uploadDate: new Date
        },{
            name: 'File 3',
            path: '.../folder/file_example_3.jpg',
            thumb: 'https://lh4.googleusercontent.com/-115_BjIc-w0/VKwr3Z2GkuI/AAAAAAAAGCE/x9j8kkCrzhM/s1600/files_app_icon_256.png',
            lastModification: new Date,
            size: 12300000,
            uploadDate: new Date
        }];

        vm.storageData = {
          limit: 1000000000,
          used: 200000000
        };

        activate();

        function activate() {}
    }
})();
